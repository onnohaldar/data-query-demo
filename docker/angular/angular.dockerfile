FROM node:current-alpine3.14

# Install global Angular CLI
RUN npm install -g @angular/cli

# Development Volume
RUN mkdir -p /angular/dev
VOLUME /angular/dev

# Install and Start Configuration
RUN mkdir -p /angular/config
COPY ./config/package.json /angular/config 
WORKDIR /angular/config
RUN npm install
EXPOSE 4200
CMD [ "npm", "start"]