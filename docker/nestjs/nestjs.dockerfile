FROM node:current-alpine3.14

# Install global NestJs CLI
RUN npm i -g @nestjs/cli

# Generic Running Configurations
RUN mkdir -p /nestjs/config
COPY ./config/* /nestjs/config

# Development Volume
RUN mkdir -p /nestjs/dev
VOLUME /nestjs/dev

# Start Configuration
EXPOSE 3000
WORKDIR /nestjs/config
CMD [ "npm", "start"]