# NestJS Docker

NestJS as a Api Server example references:

- https://docs.nestjs.com


## Setup

- [NestJS Config](config/package.json) NestJs Running Configurations
- [Dev Folder](dev) with NestJs Test Project Setup (created by using `nestjs new <project>`)
- [NestJs Docker File](nestjs.dockerfile) Container Build Config
- [NestJs Dev Docker Compose File](nestjs-dev.yml) Test Development Server Configuration