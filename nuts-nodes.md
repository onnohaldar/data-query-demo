# Nuts Node Netwerk

Stappen om een Authority (CA), Provider en Consumer Node op te zetten op een lokaal netwerk voor ontwikkel en demo-doeleinden.

## 1. Data Authority Node

Opzetten van de Data Authoruty Node waarmee de `Credetials` mee kunnen worden uitgegeven doorloopt de volgende stappen:

1. Data Authority Organisatie Registreren
2. Authority Node vindbaar maken

### 1.1 Data Authority Organisatie Registreren

Registratie van de Organisatie die de `Credentials` gaat uitgeven doorloopt de volgende stappen:

1. DID-Document aanmaken
2. Contactinfo toevoegen

#### 1.1.1 DID-Document aanmaken
  
Zie [Create and Store a Vendor DID](https://nuts-node.readthedocs.io/en/latest/pages/getting-started/4-connecting-crm.html#create-and-store-a-vendor-did).
  

Voer onderstaande `Request` uit om een DID-Document in de VDR te creeren:

```shell
curl -X 'POST' \
  'http://localhost:1323/internal/vdr/v1/did' \
  -H 'accept: application/json' \
  -H 'Content-Type: application/json' \
  -d '{
    "selfControl": true,
    "keyAgreement": true,
    "assertionMethod": true,
    "capabilityInvocation": true
  }'
```

Als `Response` zou dit een DID-Document met als `"id"`-waarde de `<authority-did>` (voorbeeld = `did:nuts:GgGEV9tpcVnMdjbDj8KEiGrkg2b6BVAdA6vUZ43UqFxn`) moeten teruggeven. *NB: die kan worden gebruikt als referentie voor de vervolgstappen.*

```json
{
  "@context": [ "https://www.w3.org/ns/did/v1" ],
  "id": "<authority-did>",
  "verificationMethod": [
    {
      "id": "did:nuts:...",
      "controller": "did:nuts:...",
      "type": "JsonWebKey2020",
      "publicKeyJwk": {
        "crv": "P-256",
        "x": "...",
        "y": "...",
        "kty": "EC"
      }
    }],
  "capabilityInvocation": [
    "did:nuts:..."
  ],
  "assertion": [
    "did:nuts:..."
  ],
  "keyAgreement": [
    "did:nuts:..."
  ],
  "service": []
}
```

### 1.1.2 Contactinfo toevoegen

Zie [Setting vendor contact information](https://nuts-node.readthedocs.io/en/latest/pages/getting-started/4-connecting-crm.html#setting-vendor-contact-information).

Voer onderstaande `Request` uit om de Contactgegevens voor de eerdere aangemaakte `<authority-did>` op te geven:

```shell
curl -X 'PUT' \
  'http://localhost:1323/internal/didman/v1/did/<authority-did>/contactinfo' \
  -H 'accept: application/json' \
  -H 'Content-Type: application/json' \
  -d '{
    "name": "Data Authority",
    "phone": "06-12345678",
    "email": "data@authority.sample",
    "website": "https://uthority.sample"
  }'
```

Als `Response` zou dit de opgegeven Contactgegevens moeten teruggeven.

```json
{
  "email":"data@authority.sample",
  "name":"Data Authority",
  "phone":"06-12345678",
  "website":"https://uthority.sample"
}
```

### 1.2 Authority Node vindbaar maken

Zie [Node Disvovery](https://nuts-node.readthedocs.io/en/latest/pages/getting-started/3-configure-your-node.html#node-discovery).

Voer onderstaande `Request` uit om de **Data Authority Node** via `<authority-did>` op het lokale Nuts Netwerk (`"type": "NutsComm"`)  vindbaar te maken:

```shell
curl -X 'POST' \
  'http://localhost:1323/internal/didman/v1/did/<authority-did>/endpoint' \
  -H 'accept: application/json' \
  -H 'Content-Type: application/json' \
  -d '{
    "type": "NutsComm",
    "endpoint": "grpc://host.docker.internal:5555"
  }'
```

Als `Response` zou dit de opgegeven `NutsComm Endpoint` moeten teruggeven.

```json
{
  "id":"<authority-did>",
  "serviceEndpoint":"grpc://host.docker.internal:5555",
  "type":"NutsComm"
}
```

## Data Provider Node


## Data Consumer Node



## Rererenties:
- [Nuts Docs beschrijving Node Configuratie](https://nuts-node.readthedocs.io/en/latest/pages/getting-started/3-configure-your-node.html)
- [Development CA Keys Genereren](https://github.com/nuts-foundation/nuts-development-network-ca)
