# Data Query Demo

Examples to create NestJS BackEnd and Angular FrontEnd in Docker Containers

- [Setup Notes](SETUP.md)
- [NestJS Docker](nestjs-docker/nestjs-docker.md)
- [Angular Notes](angular/ANGULAR.md)
- [TestCases](TESTCASES.md)
