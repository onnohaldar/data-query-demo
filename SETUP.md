# Setup

Setup documentation example in Docker with:

- NestJS as a Api Server
  - https://github.com/DataBorg/client as SparQL Client
  - Fuseki as TrippleStore
- Angular as a Web Client

# GitLab Container Registry References

https://docs.gitlab.com/ee/user/packages/container_registry/index.html#authenticate-with-the-container-registry

See `build` and `push` scrips in [NPM PACKAGE](PACKAGE.json)

docker login registry.gitlab.com

# Docker References

Environment variables
- https://docs.docker.com/compose/environment-variables/#set-environment-variables-in-containers

Dockerfile commands
- https://www.geeksforgeeks.org/difference-between-the-copy-and-add-commands-in-a-dockerfile/
  - COPY always use this for local copy actions (explicit)
  - ADD use this for external sources with URL (and .tar extractions needed)

Docker Compose
- https://gdevillele.github.io/compose/compose-file/#/ports
  - ports: `[host]:[container]`

Fuseki Image
- https://hub.docker.com/r/secoresearch/fuseki
- Use volume/bind mount for the directory /fuseki-base/databases