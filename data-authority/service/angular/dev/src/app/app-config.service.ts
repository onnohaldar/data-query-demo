import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable()
export class AppConfigService {
  constructor(private http: HttpClient) { }

  private readonly nodeDidUrl = './assets/node-did.json';

  getNutsNodeId() {
    return new Observable<string>(observer => {
      this.http.get<any>(this.nodeDidUrl).subscribe(
        nodeDID => {
          if (nodeDID.id) {
            observer.next(nodeDID.id);
          } else {
            observer.error('nodeDID does not containg <id> value!')
          }
        }
      );
    });
  }


}
