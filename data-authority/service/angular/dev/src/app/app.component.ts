import { Component, OnInit } from '@angular/core';
import { AppConfigService } from './app-config.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'Data Authority Client';

  constructor(
    private config: AppConfigService
  ) {}

  ngOnInit() {
    this.networkConfig()
  }

  private networkConfig() {
    this.config.getNutsNodeId().subscribe(nodeId => console.log(nodeId));

  }
  
}
